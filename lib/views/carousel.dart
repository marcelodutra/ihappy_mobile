import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class SlideShow extends StatefulWidget {
  @override
  _SlideShowState createState() => _SlideShowState();
}

class _SlideShowState extends State<SlideShow> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CarouselSlider(
        items: <Widget>[
          Image.asset('assets/images/capa_1.jpg'),
          Image.asset('assets/images/capa_2.jpg'),
        ],
        enableInfiniteScroll: true,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        pauseAutoPlayOnTouch: Duration(seconds: 5),
        viewportFraction: 0.9999,        
      ),
    );
  }
}
