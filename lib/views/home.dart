import 'package:flutter/material.dart';
import './carousel.dart';
import './itenshome.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Image.asset("assets/images/logo_ihappy.png"),
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: ()=>{},
          ),
        ],
      ),
      body: SingleChildScrollView(
        child:
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SlideShow(),
                ItensHome(imgEstab: 'assets/images/img_zero.png'),
                ItensHome(imgEstab: 'assets/images/img_buteco.png'),
                ItensHome(imgEstab: 'assets/images/img_emporium.png'),
                ItensHome(imgEstab: 'assets/images/img_absoluto.png'),
              ],
            ),
          ),
      ), 
      bottomNavigationBar:BottomNavigationBar(
        backgroundColor: Color.fromARGB(255, 0, 0, 0),
        type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.settings_cell,color: Color.fromARGB(255, 255, 0, 0)),
            title: new Text('Destaques',
              style: TextStyle(
                color: Color.fromARGB(255, 255, 0, 0),
              ),
            ),
            backgroundColor: Color.fromARGB(255, 25, 25, 25),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu,color: Color.fromARGB(255, 227, 227, 227)),
            title: new Text('Categorias',
              style: TextStyle(
                color: Color.fromARGB(255, 227, 227, 227),
              ),
            )
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.payment,color: Color.fromARGB(255, 227, 227, 227)),
            title: new Text('Carteira',
              style: TextStyle(
                color: Color.fromARGB(255, 227, 227, 227),
              ),
            )
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.live_help,color: Color.fromARGB(255, 227, 227, 227)),
            title: new Text('Ajuda',
              style: TextStyle(
                color: Color.fromARGB(255, 227, 227, 227),
              ),
            )
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person,color: Color.fromARGB(255, 227, 227, 227)),
            title: new Text('Perfil',
              style: TextStyle(
                color: Color.fromARGB(255, 0, 0, 0),
              ),
            )
          )
        ],
      )// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}