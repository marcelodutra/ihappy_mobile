import 'package:flutter/material.dart';

class ItensHome extends StatelessWidget {

  final String imgEstab;

  ItensHome({this.imgEstab});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15, bottom: 15),
      color: Colors.grey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset(this.imgEstab, width: 100,),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                    child:
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                color: Colors.grey[800],
                              ),
                              Text('QUA-DOM',
                                style: TextStyle(
                                  color: Colors.grey[800],
                                  fontSize: 12,
                                ),
                              ),
                              Icon(
                                Icons.pin_drop,
                                color: Colors.grey[800],
                              ),
                              Text('Rua tal, nº 000'),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: Text('Música Ao Vivo', 
                              style: TextStyle(
                                color: Colors.red[800],
                                fontSize: 18,
                              ),
                            ),
                          ),
                          Text('Comprar seu combo!', 
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ) 
    );
  }
}




/*
class ItensHome extends StatefulWidget {

  final String imgEstab;

  ItensHome({this.imgEstab});

  @override
  _ItensHomeState createState() => _ItensHomeState();

}

class _ItensHomeState extends State<ItensHome> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15, right: 15),
      color: Colors.grey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset('assets/images/img_zero.png', width: 100,),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                    child:
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                color: Colors.grey[800],
                              ),
                              Text('QUA-DOM',
                                style: TextStyle(
                                  color: Colors.grey[800],
                                  fontSize: 12,
                                ),
                              ),
                              Icon(
                                Icons.pin_drop,
                                color: Colors.grey[800],
                              ),
                              Text('Rua tal, nº 000'),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: Text('Música Ao Vivo', 
                              style: TextStyle(
                                color: Colors.red[800],
                                fontSize: 18,
                              ),
                            ),
                          ),
                          Text('Comprar seu combo!', 
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ) 
    );
  }
}*/